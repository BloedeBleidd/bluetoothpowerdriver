################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../serialDataWithDMA/interfaceDMA.cpp \
../serialDataWithDMA/serialDataDMA.cpp 

OBJS += \
./serialDataWithDMA/interfaceDMA.o \
./serialDataWithDMA/serialDataDMA.o 

CPP_DEPS += \
./serialDataWithDMA/interfaceDMA.d \
./serialDataWithDMA/serialDataDMA.d 


# Each subdirectory must supply rules for building sources it contributes
serialDataWithDMA/%.o: ../serialDataWithDMA/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -O3 -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra -DUSE_STDPERIPH_DRIVER -DSTM32F1 -DSTM32F10X_MD -I"C:\Prog\workspace\RTOS\include" -I"C:\Prog\workspace\RTOS\RTOS\include" -I"C:\Prog\workspace\RTOS\serialDataWithDMA" -I"C:\Prog\workspace\RTOS\StdPeriph_Driver\inc" -I"C:\Prog\workspace\RTOS\CMSIS\core" -I"C:\Prog\workspace\RTOS\CMSIS\device" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


