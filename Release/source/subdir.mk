################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/gpio.c \
../source/task_status_led.c 

CPP_SRCS += \
../source/dynamicAllocation.cpp \
../source/main.cpp \
../source/task_main.cpp \
../source/task_serial.cpp 

OBJS += \
./source/dynamicAllocation.o \
./source/gpio.o \
./source/main.o \
./source/task_main.o \
./source/task_serial.o \
./source/task_status_led.o 

C_DEPS += \
./source/gpio.d \
./source/task_status_led.d 

CPP_DEPS += \
./source/dynamicAllocation.d \
./source/main.d \
./source/task_main.d \
./source/task_serial.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -O3 -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra -DUSE_STDPERIPH_DRIVER -DSTM32F1 -DSTM32F10X_MD -I"C:\Prog\workspace\RTOS\include" -I"C:\Prog\workspace\RTOS\RTOS\include" -I"C:\Prog\workspace\RTOS\serialDataWithDMA" -I"C:\Prog\workspace\RTOS\StdPeriph_Driver\inc" -I"C:\Prog\workspace\RTOS\CMSIS\core" -I"C:\Prog\workspace\RTOS\CMSIS\device" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O3 -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra -DUSE_STDPERIPH_DRIVER -DSTM32F1 -DSTM32F10X_MD -I"C:\Prog\workspace\RTOS\include" -I"C:\Prog\workspace\RTOS\RTOS\include" -I"C:\Prog\workspace\RTOS\serialDataWithDMA" -I"C:\Prog\workspace\RTOS\StdPeriph_Driver\inc" -I"C:\Prog\workspace\RTOS\CMSIS\core" -I"C:\Prog\workspace\RTOS\CMSIS\device" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


