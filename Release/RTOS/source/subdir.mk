################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../RTOS/source/croutine.c \
../RTOS/source/event_groups.c \
../RTOS/source/heap_4.c \
../RTOS/source/list.c \
../RTOS/source/port.c \
../RTOS/source/queue.c \
../RTOS/source/stream_buffer.c \
../RTOS/source/tasks.c \
../RTOS/source/timers.c 

OBJS += \
./RTOS/source/croutine.o \
./RTOS/source/event_groups.o \
./RTOS/source/heap_4.o \
./RTOS/source/list.o \
./RTOS/source/port.o \
./RTOS/source/queue.o \
./RTOS/source/stream_buffer.o \
./RTOS/source/tasks.o \
./RTOS/source/timers.o 

C_DEPS += \
./RTOS/source/croutine.d \
./RTOS/source/event_groups.d \
./RTOS/source/heap_4.d \
./RTOS/source/list.d \
./RTOS/source/port.d \
./RTOS/source/queue.d \
./RTOS/source/stream_buffer.d \
./RTOS/source/tasks.d \
./RTOS/source/timers.d 


# Each subdirectory must supply rules for building sources it contributes
RTOS/source/%.o: ../RTOS/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O3 -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra -DUSE_STDPERIPH_DRIVER -DSTM32F1 -DSTM32F10X_MD -I"C:\Prog\workspace\RTOS\include" -I"C:\Prog\workspace\RTOS\RTOS\include" -I"C:\Prog\workspace\RTOS\serialDataWithDMA" -I"C:\Prog\workspace\RTOS\StdPeriph_Driver\inc" -I"C:\Prog\workspace\RTOS\CMSIS\core" -I"C:\Prog\workspace\RTOS\CMSIS\device" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


