/*
 * task_main.h
 *
 *  Created on: 01.08.2019
 *      Author: BloedeBleidd
 */

#ifndef TASK_MAIN_H_
#define TASK_MAIN_H_

#include <stdint.h>

const uint16_t stackMain = 400;

void taskMain ( void * param );

#endif /* TASK_MAIN_H_ */
