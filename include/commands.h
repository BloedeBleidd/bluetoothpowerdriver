/*
 * commands.h
 *
 *  Created on: 01.08.2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef COMMANDS_H_
#define COMMANDS_H_

#include <stdlib.h>
#include <stdint.h>

enum CommandType { HELP, ERROR_GLOBAL, ERROR_OUTPUT, ERROR_SPEED, ERROR_MOTOR, SM1OK, SM2OK, SM1SPEED, SM2SPEED, SM1MOVE, SM2MOVE, ON_OUTPUT, OFF_OUTPUT };

struct Command
{
	CommandType type;
	int32_t value;
};


#endif /* COMMANDS_H_ */
