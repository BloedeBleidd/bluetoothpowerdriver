/*
 * task_serial.h
 *
 *  Created on: 01.08.2019
 *      Author: BloedeBleidd
 */

#ifndef TASK_SERIAL_H_
#define TASK_SERIAL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

const uint16_t stackSerial = 200;

void taskSerial ( void * param );

#ifdef __cplusplus
}
#endif


#endif /* TASK_SERIAL_H_ */
