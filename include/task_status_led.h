/*
 * status_led.h
 *
 *  Created on: 01.08.2019
 *      Author: BloedeBleidd
 */

#ifndef TASK_STATUS_LED_H_
#define TASK_STATUS_LED_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

const static uint16_t stackStatusLed = 30;

void taskStatusLed ( void * param );

#ifdef __cplusplus
}
#endif


#endif /* TASK_STATUS_LED_H_ */
