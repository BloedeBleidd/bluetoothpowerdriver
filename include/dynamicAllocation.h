/*
 * dynamicAllocation.h
 *
 *  Created on: Feb 9, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#ifndef DYNAMICALLOCATION_H_
#define DYNAMICALLOCATION_H_

#include "RTOS_includes.h"

void * operator new( size_t size );

void operator delete( void * ptr ) noexcept ;


#endif /* DYNAMICALLOCATION_H_ */
