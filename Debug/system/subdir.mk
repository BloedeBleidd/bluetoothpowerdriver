################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/syscalls.c \
../system/system_stm32f10x.c 

S_UPPER_SRCS += \
../system/startup_stm32.S 

OBJS += \
./system/startup_stm32.o \
./system/syscalls.o \
./system/system_stm32f10x.o 

S_UPPER_DEPS += \
./system/startup_stm32.d 

C_DEPS += \
./system/syscalls.d \
./system/system_stm32f10x.d 


# Each subdirectory must supply rules for building sources it contributes
system/%.o: ../system/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -x assembler-with-cpp -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F1 -DSTM32F10X_MD -I"C:\Prog\workspace\bluetoothPowerBoard\include" -I"C:\Prog\workspace\bluetoothPowerBoard\RTOS\include" -I"C:\Prog\workspace\bluetoothPowerBoard\serialDataWithDMA" -I"C:\Prog\workspace\bluetoothPowerBoard\StdPeriph_Driver\inc" -I"C:\Prog\workspace\bluetoothPowerBoard\CMSIS\core" -I"C:\Prog\workspace\bluetoothPowerBoard\CMSIS\device" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

system/%.o: ../system/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F1 -DSTM32F10X_MD -I"C:\Prog\workspace\bluetoothPowerBoard\include" -I"C:\Prog\workspace\bluetoothPowerBoard\RTOS\include" -I"C:\Prog\workspace\bluetoothPowerBoard\serialDataWithDMA" -I"C:\Prog\workspace\bluetoothPowerBoard\StdPeriph_Driver\inc" -I"C:\Prog\workspace\bluetoothPowerBoard\CMSIS\core" -I"C:\Prog\workspace\bluetoothPowerBoard\CMSIS\device" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


