################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../serialDataWithDMA/interfaceDMA.cpp \
../serialDataWithDMA/serialDataDMA.cpp 

OBJS += \
./serialDataWithDMA/interfaceDMA.o \
./serialDataWithDMA/serialDataDMA.o 

CPP_DEPS += \
./serialDataWithDMA/interfaceDMA.d \
./serialDataWithDMA/serialDataDMA.d 


# Each subdirectory must supply rules for building sources it contributes
serialDataWithDMA/%.o: ../serialDataWithDMA/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F1 -DSTM32F10X_MD -I"C:\Prog\workspace\bluetoothPowerBoard\include" -I"C:\Prog\workspace\bluetoothPowerBoard\RTOS\include" -I"C:\Prog\workspace\bluetoothPowerBoard\serialDataWithDMA" -I"C:\Prog\workspace\bluetoothPowerBoard\StdPeriph_Driver\inc" -I"C:\Prog\workspace\bluetoothPowerBoard\CMSIS\core" -I"C:\Prog\workspace\bluetoothPowerBoard\CMSIS\device" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


