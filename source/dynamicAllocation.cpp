/*
 * dynamicAllocation.cpp
 *
 *  Created on: Feb 9, 2019
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include "dynamicAllocation.h"
#include "RTOS_includes.h"

void * operator new( size_t size )
{
	return pvPortMalloc(size);
}

/*
void * operator new
{
	return pvPortMalloc( size );
}
*/

void operator delete( void * ptr ) noexcept
{
	vPortFree( ptr );
}

/*
void operator delete
{
	vPortFree( ptr );
}
*/
