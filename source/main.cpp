#include "stm32f10x.h"
#include "RTOS_includes.h"
#include "task_status_led.h"
#include "task_main.h"
#include "task_serial.h"

int main(void)
{
	xTaskCreate( taskStatusLed, "status led", stackStatusLed, NULL, tskLOW_PRIORITY, NULL );
	xTaskCreate( taskMain, "main", stackMain, NULL, tskHIGH_PRIORITY, NULL );
	xTaskCreate( taskSerial, "serial", stackSerial, NULL, tskNORMAL_PRIORITY, NULL );
	vTaskStartScheduler();

	return 0;
}
