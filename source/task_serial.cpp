/*
 * task_serial.c
 *
 *  Created on: 01.08.2019
 *      Author: BloedeBleidd
 */

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "RTOS_includes.h"
#include "task_serial.h"
#include "serialDataDMA.h"
#include "gpio.h"
#include "commands.h"
#include <stdlib.h>
#include <string>
#include <string.h>

void (*bootJump) (void );

extern QueueHandle_t queueSerialDataReceive;
extern QueueHandle_t queueSerialDataTransmit;
SemaphoreHandle_t semaphoreDMA;

void usartSerialDataInitialize( USART_TypeDef *USARTx, uint32_t baudRate );
void taskDMA( void * param );

void taskSerial( void * param )
{
	(void)(param);
	const TickType_t DELAY = 2;

	Command command;
	string strCmd;
	strCmd.reserve( 200 );

	usartSerialDataInitialize( USART1, 38400 );
	serialDataDMA bt( DMA1, DMA1_Channel4, (uint32_t)&USART1->DR, 400, DMA1_Channel5, (uint32_t)&USART1->DR, 2000 );

	semaphoreDMA = xSemaphoreCreateBinary();
	xTaskCreate( taskDMA, "dma", configMINIMAL_STACK_SIZE, &bt, tskHIGH_PRIORITY, NULL );

/*	//hc05 set baud
	strCmd = "AT+UART?\r\n";			bt.addCommand( strCmd );
	strCmd = "AT+UART=38400,0,2\r\n";	bt.addCommand( strCmd );
	strCmd = "AT+UART?\r\n";			bt.addCommand( strCmd );*/

	TickType_t lastTime = xTaskGetTickCount();

	for(;;)
	{
		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS(DELAY) );

		//volatile size_t tmp = xPortGetMinimumEverFreeHeapSize();

		if( xQueueReceive( queueSerialDataTransmit, (void *) &command, 0 ) )
		{
			char strVal[15];
			itoa( command.value, strVal, 10 );

			switch( command.type )
			{
				case SM1OK: strCmd = "SM1.OK\r"; break;
				case SM2OK: strCmd = "SM2.OK\r"; break;
				case ON_OUTPUT: strCmd = "ON."; strCmd += strVal; strCmd += '\r'; break;
				case OFF_OUTPUT: strCmd = "OFF."; strCmd += strVal; strCmd += '\r'; break;
				case SM1SPEED: strCmd = "SM1SPEED."; strCmd += strVal; strCmd += '\r'; break;
				case SM2SPEED: strCmd = "SM2SPEED."; strCmd += strVal; strCmd += '\r'; break;
				case HELP: strCmd = "onX - output nr X= true\r\noffX - output nr X = false\r"; break;
				case ERROR_OUTPUT: strCmd = "OUTPUT ERROR\r"; break;
				case ERROR_MOTOR: strCmd = "MOTOR ERROR\r"; break;
				default : strCmd = "ERROR\r"; break;
			}

			bt.addCommand( strCmd );
		}

		if( bt.getCommand( strCmd ) )
		{
			if( std::string::npos != strCmd.find( "on." ) )
				command = { ON_OUTPUT, std::atoi( strCmd.c_str()+3 ) };
			else if( std::string::npos != strCmd.find( "off." ) )
				command = { OFF_OUTPUT, std::atoi( strCmd.c_str()+4 ) };
			else if( std::string::npos != strCmd.find( "sm1." ) )
				command = { SM1MOVE, std::atoi( strCmd.c_str()+4 ) };
			else if( std::string::npos != strCmd.find( "sm2." ) )
				command = { SM2MOVE, std::atoi( strCmd.c_str()+4 ) };
			else if( std::string::npos != strCmd.find( "sm1speed." ) )
				command = { SM1SPEED, std::atoi( strCmd.c_str()+9 ) };
			else if( std::string::npos != strCmd.find( "sm2speed." ) )
				command = { SM2SPEED, std::atoi( strCmd.c_str()+9 ) };
			else if( std::string::npos != strCmd.find( "sm1speed" ) )
				command = { SM1SPEED, 0 };
			else if( std::string::npos != strCmd.find( "sm2speed" ) )
				command = { SM2SPEED, 0 };
			else if( std::string::npos != strCmd.find( "sm" ) )
				command = { ERROR_MOTOR, 0 };
			else
				command = { ERROR_GLOBAL, 0 };

			if( ( command.type == ON_OUTPUT || command.type == OFF_OUTPUT ) && ( command.value > 12 || command.value <= 0 ) )
				command.type = ERROR_OUTPUT;

			if( ( command.type == SM1SPEED || command.type == SM2SPEED ) && command.value < 0 )
				command.type = ERROR_SPEED;

			if( std::string::npos != strCmd.find( "FirmwareUpdate" ) )
			{
				const uint8_t UPDATE_REQUESTS = 4;
				static uint8_t updateRequests = 0;

				if( updateRequests < UPDATE_REQUESTS )
				{
					char strVal[3];
					itoa( UPDATE_REQUESTS - updateRequests, strVal, 10 );
					strcat( strVal, "\r" );
					strCmd = "Firmware Update requests left = ";
					strCmd += strVal;
					bt.addCommand( strCmd );
					updateRequests++;
				}
				else
				{
					strCmd = "I am jumping to bootloader\r";
					bt.addCommand( strCmd );
					vTaskDelay( pdMS_TO_TICKS( 10 ) );
					RCC->APB2RSTR |= RCC_APB2RSTR_USART1RST;
					vTaskDelay( pdMS_TO_TICKS( 2 ) );
					RCC->APB2RSTR &= ~RCC_APB2RSTR_USART1RST;
					vTaskDelay( pdMS_TO_TICKS( 2 ) );
					vTaskSuspendAll();
					RCC_DeInit();
					SysTick->CTRL = 0;
					SysTick->VAL = 0;
					SysTick->LOAD = 0;
					__disable_irq();
					__set_MSP( 0x20005000 ); // _estack linker script
					bootJump = ( void (*) (void) ) ( *( (uint32_t *)0x1ffff004 ) );
					bootJump();
				}
			}
			else
				xQueueSend( queueSerialDataReceive, (void *) &command, 0 );
		}
	}
}

void taskDMA( void * param )
{
	(void)(param);

	for(;;)
		if( xSemaphoreTake( semaphoreDMA, portMAX_DELAY ) == pdTRUE )
			((serialDataDMA*)param)->dmaEndTransmitInterruptHandler();
}

extern "C"
{
	void DMA1_Channel4_IRQHandler (void)
	{
		static BaseType_t xHigherPriorityTaskWoken;
		xHigherPriorityTaskWoken = pdFALSE;

		if(DMA1->ISR & DMA_ISR_TCIF4)
		{
			DMA1->IFCR |= DMA_IFCR_CTCIF4;
			xSemaphoreGiveFromISR( semaphoreDMA, &xHigherPriorityTaskWoken );
		}

		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
}

void usartSerialDataInitialize( USART_TypeDef* USARTx, uint32_t baudRate )
{
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;// | RCC_APB2ENR_AFIOEN;
//	AFIO->MAPR |= AFIO_MAPR_USART1_REMAP;
	gpioPinConfiguration( GPIOA, 9, GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED );
	gpioPinConfiguration( GPIOA, 10, GPIO_MODE_INPUT_FLOATING );

	USARTx->BRR = SystemCoreClock/baudRate;
	USARTx->CR3 = USART_CR3_DMAR | USART_CR3_DMAT;
	USARTx->CR1 = USART_CR1_UE | USART_CR1_TE | USART_CR1_RE | USART_CR1_M | USART_CR1_PCE;

	NVIC_EnableIRQ( DMA1_Channel4_IRQn );
	NVIC_SetPriority( DMA1_Channel4_IRQn, 11 );
}

