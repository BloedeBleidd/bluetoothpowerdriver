/*
 * status_led.c
 *
 *  Created on: 01.08.2019
 *      Author: BloedeBleidd
 */

#include "stm32f10x.h"
#include "RTOS_includes.h"
#include "task_status_led.h"
#include "gpio.h"

void taskStatusLed ( void * param )
{
	(void)(param);
	const TickType_t DELAY_ON = 50;
	const TickType_t DELAY_OFF1 = 100, DELAY_OFF2 = 1800;

	gpioPinConfiguration( GPIOA, 8, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED);
	TickType_t lastTime = xTaskGetTickCount();

	for(;;)
	{
		gpioBitSet( GPIOA, 8 );
		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS(DELAY_ON) );

		gpioBitReset( GPIOA, 8 );
		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS(DELAY_OFF1) );

		gpioBitSet( GPIOA, 8 );
		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS(DELAY_ON) );

		gpioBitReset( GPIOA, 8 );
		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS(DELAY_OFF2) );
	}
}


