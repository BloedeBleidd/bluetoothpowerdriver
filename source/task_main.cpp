/*
 * task_main.cpp
 *
 *  Created on: 01.08.2019
 *      Author: BloedeBleidd
 */


#include "stm32f10x.h"
#include "RTOS_includes.h"
#include "gpio.h"
#include <string>
#include "commands.h"

struct MotorProcess
{
	int32_t steps;
	TickType_t delay;
};

QueueHandle_t queueSerialDataReceive;
QueueHandle_t queueSerialDataTransmit;
TaskHandle_t taskMotor1Handle, taskMotor2Handle;
QueueHandle_t queueMotor1, queueMotor2;

void motorProcess( int32_t &position, MotorProcess &process, GPIO_TypeDef * const GPIOx, PinNumber PIN1, PinNumber PIN2, PinNumber PIN3, PinNumber PIN4 )
{
	TickType_t lastTime = xTaskGetTickCount();

	while( process.steps != 0 )
	{
		GpioPort port = gpioPortRead( GPIOx ) & ~(( 1 << PIN1 ) | ( 1 << PIN2 ) | ( 1 << PIN3 ) | ( 1 << PIN4 ));

		if( position == 0 )			gpioPortWrite( GPIOx, port | ( 1 << PIN1 ) );
		else if( position == 1 )	gpioPortWrite( GPIOx, port | ( 1 << PIN2 ) );
		else if( position == 2 )	gpioPortWrite( GPIOx, port | ( 1 << PIN3 ) );
		else if( position == 3 )	gpioPortWrite( GPIOx, port | ( 1 << PIN4 ) );

		if( process.steps > 0 )
		{
			position++;
			process.steps--;
		}
		else
		{
			position--;
			process.steps++;
		}

		if( position > 3 )
			position = 0;
		else if( position < 0 )
			position = 3;

		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS( process.delay ) );
	}
}

void taskMotor1( void * param )
{
	(void)(param);
	int32_t position = 0;
	MotorProcess process;
	const Command cmd = { SM1OK, 0 };

	for(;;)
	{
		if( xQueueReceive( queueMotor1, (void *) &process, portMAX_DELAY ) )
		{
			motorProcess( position, process, GPIOB, 15, 14, 13, 12 );
			xQueueSend( queueSerialDataTransmit, (void *) &cmd, 0 );
		}
	}
}

void taskMotor2( void * param )
{
	(void)(param);
	int32_t position = 0;
	MotorProcess process;
	const Command cmd = { SM2OK, 0 };

	for(;;)
	{
		if( xQueueReceive( queueMotor2, (void *) &process, portMAX_DELAY ) )
		{
			motorProcess( position, process, GPIOB, 7, 6, 5, 4 );
			xQueueSend( queueSerialDataTransmit, (void *) &cmd, 0 );
		}
	}
}


void taskMain ( void * param )
{
	(void)(param);
	TickType_t delay1 = 10, delay2 = 10;
	Command command;
	MotorProcess process;

	//step motors
	for( uint32_t i=0; i<4; i++ )
	{
		gpioPinConfiguration( GPIOB, 12+i, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED );
		gpioPinConfiguration( GPIOB, 4+i, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED );
	}

	//power outputs
	for( uint32_t i=1; i<8; i++ )
		gpioPinConfiguration( GPIOA, i, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED );
	for( uint32_t i=0; i<2; i++ )
		gpioPinConfiguration( GPIOB, i, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED );
	for( uint32_t i=10; i<12; i++ )
		gpioPinConfiguration( GPIOB, i, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED );

	queueMotor1 = xQueueCreate( 100, sizeof( MotorProcess ) );
	queueMotor2 = xQueueCreate( 100, sizeof( MotorProcess ) );
	xTaskCreate( taskMotor1, "m1", configMINIMAL_STACK_SIZE, NULL, tskHIGHEST_PRIORITY, &taskMotor1Handle );
	xTaskCreate( taskMotor2, "m2", configMINIMAL_STACK_SIZE, NULL, tskHIGHEST_PRIORITY, &taskMotor2Handle );
	queueSerialDataReceive = xQueueCreate( 100, sizeof( Command ) );
	queueSerialDataTransmit = xQueueCreate( 100, sizeof( Command ) );
	vTaskPrioritySet( NULL, tskNORMAL_PRIORITY );

	for(;;)
	{
		if( xQueueReceive( queueSerialDataReceive, (void *) &command, portMAX_DELAY ) )
		{
			if( command.type == ON_OUTPUT )
			{
				if( command.value < 8 )
					gpioBitSet( GPIOA, command.value );
				else if( command.value < 10 )
					gpioBitSet( GPIOB, command.value-8 );
				else if( command.value <= 12 )
					gpioBitSet( GPIOB, command.value-1 );

				xQueueSend( queueSerialDataTransmit, (void *) &command, 0 );
			}
			else if( command.type == OFF_OUTPUT )
			{
				if( command.value < 8 )
					gpioBitReset( GPIOA, command.value );
				else if( command.value < 10 )
					gpioBitReset( GPIOB, command.value-8 );
				else if( command.value <= 12 )
					gpioBitReset( GPIOB, command.value-1 );

				xQueueSend( queueSerialDataTransmit, (void *) &command, 0 );
			}
			else if( command.type == SM1SPEED )
			{
				if( command.value == 0 )
				{
					command.value = delay1;
					xQueueSend( queueSerialDataTransmit, (void *) &command, 0 );
				}
				else
					delay1 = command.value;
			}
			else if( command.type == SM2SPEED )
			{
				if( command.value == 0 )
				{
					command.value = delay2;
					xQueueSend( queueSerialDataTransmit, (void *) &command, 0 );
				}
				else
					delay2 = command.value;
			}
			else if( command.type == SM1MOVE )
			{
				process.delay = delay1;
				process.steps = command.value;
				xQueueSend( queueMotor1, (void *) &process, 0 );
			}
			else if( command.type == SM2MOVE )
			{
				process.delay = delay2;
				process.steps = command.value;
				xQueueSend( queueMotor2, (void *) &process, 0 );
			}
			else if( command.type == ERROR_OUTPUT || command.type == ERROR_MOTOR || command.type == ERROR_GLOBAL || command.type == ERROR_SPEED )
			{
				xQueueSend( queueSerialDataTransmit, (void *) &command, 0 );
			}
		}
	}
}

