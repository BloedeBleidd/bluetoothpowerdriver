#include <string>
#include "stm32f10x.h"

using namespace std;

// ASCII signs for identify start and end of commands
const char asciiStartOfText       = '~';
const char asciiEndOfText         = '@';
const char asciiStartOfCommand    = '\r';
const char asciiEndOfCommand      = 0; // null

typedef int16_t WORD_t;

typedef struct
{
    WORD_t length;
    char *str;
}transmitData_t;


class serialDataDMA
{
private:
    transmitData_t *transmitData;
    char *receiveData;

    WORD_t transmitBufferLength;
    WORD_t receiveBufferLength;
    WORD_t transmitCntAmountOfCmd;
    WORD_t transmitCntHead;
    WORD_t transmitCntTail;
    WORD_t receiveCmdStartPosition;
    WORD_t receiveCmdEndPosition;

    DMA_Channel_TypeDef *transmitDmaChannel;
    DMA_Channel_TypeDef *receiveDmaChannel;
    DMA_TypeDef *transmitDMA;

    bool findStartOfReceivedCommand();
    bool findEndOfReceivedCommand();
    bool findReceivedCommand();
    void convertReceivedCommand();
    bool searchReceivedCommand();
    void dmaReceiveInitialize(uint32_t DataRegister);
    void dmaTransmitInitialize(uint32_t DataRegister);
    void dmaTransactionConfig(transmitData_t transmitData);
	bool getTransmitCommandDataPack(transmitData_t &);
	bool deleteLastTransmitedCommand();
	bool addCommandToQueue(string &);

public:
    serialDataDMA(DMA_TypeDef *DMA,DMA_Channel_TypeDef *DMA_TransmitChannel,uint32_t transmitDataRegister,WORD_t tranBufSize, DMA_Channel_TypeDef *DMA_ReceiveChannel,uint32_t receiveDataRegister,WORD_t recBufSize);
    ~serialDataDMA();

    bool getCommand(string &);
    bool addCommand(string &);
    void dmaEndTransmitInterruptHandler();
};
