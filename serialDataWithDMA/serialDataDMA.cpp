#include "serialDataDMA.h"
#include <string>
#include <cstring>

using namespace std;


serialDataDMA::serialDataDMA(DMA_TypeDef *DMA,DMA_Channel_TypeDef *DMA_TransmitChannel,uint32_t transmitDataRegister,WORD_t tranBufSize, DMA_Channel_TypeDef *DMA_ReceiveChannel,uint32_t receiveDataRegister,WORD_t recBufSize)
{
    receiveBufferLength = recBufSize;
    receiveData = new char[receiveBufferLength];
    memset(receiveData,0,receiveBufferLength);
    receiveCmdStartPosition = 0;
    receiveCmdEndPosition = 0;

    transmitBufferLength = tranBufSize;
    transmitData = new transmitData_t[transmitBufferLength];
    memset(transmitData,0,transmitBufferLength);
    transmitCntAmountOfCmd = 0;
    transmitCntHead = 0;
    transmitCntTail = 0;

    receiveDmaChannel = DMA_ReceiveChannel;
    transmitDmaChannel = DMA_TransmitChannel;
    transmitDMA = DMA;

    RCC->AHBENR |= RCC_AHBENR_DMA1EN;
    dmaReceiveInitialize(receiveDataRegister);
    dmaTransmitInitialize(transmitDataRegister);
    //NVIC_EnableIRQ(irq);
}

serialDataDMA::~serialDataDMA()
{
    delete[] receiveData;
    delete[] transmitData;
}


bool serialDataDMA::findStartOfReceivedCommand()
{
    WORD_t i;

    for(i=receiveCmdEndPosition;i<receiveBufferLength;i++)
    {
        if(receiveData[i] == asciiStartOfText)
        {
            receiveCmdStartPosition = i;
            return true;
        }
    }
    for(i=0;i<receiveCmdEndPosition;i++)
    {
        if(receiveData[i] == asciiStartOfText)
        {
            receiveCmdStartPosition = i;
            return true;
        }
    }
    return false;
}

bool serialDataDMA::findEndOfReceivedCommand()
{
    WORD_t i;

    for(i=receiveCmdStartPosition;i<receiveBufferLength;i++)
    {
        if(receiveData[i] == asciiEndOfText)
        {
            receiveCmdEndPosition = i;
            return true;
        }
    }
    for(i=0;i<receiveCmdStartPosition;i++)
    {
        if(receiveData[i] == asciiEndOfText)
        {
            receiveCmdEndPosition = i;
            return true;
        }
    }
    return false;
}

bool serialDataDMA::findReceivedCommand()
{
    static bool findedStart = false;

    if(findedStart==false)
    {
       if(serialDataDMA::findStartOfReceivedCommand())  findedStart = true;
    }

    if(findedStart==true)
    {
        if(serialDataDMA::findEndOfReceivedCommand())
        {
            findedStart = false;
            return true;
        }
    }
    return false;
}

void serialDataDMA::convertReceivedCommand()
{
    receiveData[receiveCmdStartPosition] = asciiStartOfCommand;
    receiveData[receiveCmdEndPosition] = asciiEndOfCommand;
}

bool serialDataDMA::searchReceivedCommand()
{
    if(serialDataDMA::findReceivedCommand())
    {
        serialDataDMA::convertReceivedCommand();
        return true;
    }
    else    return false;
}

bool serialDataDMA::getCommand(string &str)
{
    if(serialDataDMA::searchReceivedCommand())
    {
        str.clear();

        if(receiveCmdEndPosition > receiveCmdStartPosition)
        {
            str.assign(&receiveData[receiveCmdStartPosition+1],&receiveData[receiveCmdStartPosition]+(receiveCmdEndPosition - receiveCmdStartPosition));
        }
        else
        {
            str.assign(&receiveData[receiveCmdStartPosition+1],&receiveData[receiveCmdStartPosition]+(receiveBufferLength - receiveCmdStartPosition));
            str.append(&receiveData[0],&receiveData[0]+receiveCmdEndPosition);
        }
        return true;
    }
    else    return false;
}


bool serialDataDMA::getTransmitCommandDataPack(transmitData_t &pack)
{
    if(transmitCntAmountOfCmd==0)    return false;

    pack.length = transmitData[transmitCntTail].length;
    pack.str = transmitData[transmitCntTail].str;

    return true;
}

bool serialDataDMA::deleteLastTransmitedCommand()
{
    if(transmitCntAmountOfCmd==0)  return false;
    delete[] transmitData[transmitCntTail].str;
    transmitCntTail++;
    if(transmitCntTail>=transmitBufferLength) transmitCntTail = 0;
    transmitCntAmountOfCmd--;
    return true;
}

bool serialDataDMA::addCommandToQueue(string &s)
{
	WORD_t len;
	char *stringPointer;

	if(transmitCntAmountOfCmd>=transmitBufferLength)    return false;

	len = s.length();
	stringPointer = new char[len];

	if(stringPointer==NULL)  return false;

	memcpy(stringPointer,s.c_str(),len);
	transmitData[transmitCntHead].length = len;
	transmitData[transmitCntHead].str = stringPointer;

	transmitCntHead++;
	if(transmitCntHead>=transmitBufferLength) transmitCntHead = 0;
	transmitCntAmountOfCmd++;

	return true;
}

bool serialDataDMA::addCommand(string &s)
{
	if(serialDataDMA::addCommandToQueue(s)==true)
	{
		if(transmitDmaChannel->CNDTR==0 && transmitCntAmountOfCmd==1)// if(((transmitDMA->ISR & transmitCompleteFLag) || (transmitDmaChannel->CNDTR==0)) && transmitCntAmountOfCmd==1)
		{
			transmitData_t data;
			serialDataDMA::getTransmitCommandDataPack(data);
			serialDataDMA::dmaTransactionConfig(data);
		}
		return true;
	}
	else
	{
		return false;
	}
}


void serialDataDMA::dmaEndTransmitInterruptHandler()
{
	transmitData_t data;

	if(serialDataDMA::deleteLastTransmitedCommand())
	{
		if(serialDataDMA::getTransmitCommandDataPack(data))
		{
			serialDataDMA::dmaTransactionConfig(data);
		}
	}
}






